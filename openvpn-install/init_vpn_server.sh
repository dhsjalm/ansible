#!/bin/bash

KEY_NAME="frankfurt-key-amazon"

aws ec2 run-instances \
  --image-id ami-0c960b947cbb2dd16 \
  --key-name "$KEY_NAME" \
  --instance-type t2.micro \
  --region eu-central-1 \
  --count 1 \
  --security-group-ids sg-4df2c82b
