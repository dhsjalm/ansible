#!/bin/bash

############################
# Simple script to backup  #
# docker container and     #
# path inside it           #
############################

# Declare some constants
DATE=$(date +%F-%R:%S)
# Declare MEGA_FLAGS massive with credentials
# Which storedd in creds.yml in ansible as encrypted value
declare -a MEGA_FLAGS
declare -a INSPECT_FLAGS
declare -a CONTAINERS_TO_BACK
MEGA_FLAGS=( -u "germanius34qw65@gmail.com" -p "FpvBxLV0uMnvSUcjy03f9VieZkVqgt" )
INSPECT_FLAGS=( --format='{{range .Mounts}}{{.Destination}}{{end}}' )
CONTAINERS_TO_BACK=("nifty_dijkstra" "thirsty_northcutt")
readonly MEGA_FLAGS
readonly INSPECT_FLAGS
readonly CONTAINERS_TO_BACK

check_dirs() {
  [[ $(megals --reload "${MEGA_FLAGS[@]}" | grep /Root/backups) ]] &&: || megamkdir "${MEGA_FLAGS[@]}" /Root/backups
}

copy_volumes() {
  local CONTAINER_NAME="$1"
  VOLUME_NAME=$(docker inspect "${INSPECT_FLAGS[@]}" "$CONTAINER_NAME")
  echo "copying $CONTAINER_NAME and volume $VOLUME_NAME"
  docker cp --archive "$CONTAINER_NAME:$VOLUME_NAME" .
  TAR_NAME="$CONTAINER_NAME-backup.tar.gz"
  tar czf $TAR_NAME "./$VOLUME_NAME"
  megamkdir "${MEGA_FLAGS[@]}" "/Root/backups/$CONTAINER_NAME-$DATE"
  megaput "${MEGA_FLAGS[@]}" "$TAR_NAME" --path "/Root/backups/$CONTAINER_NAME-$DATE"
}

clear_space() {
  rm -rf  "$TAR_NAME"
  rm -rf "./$VOLUME_NAME"
}

check_dirs

for container in ${CONTAINERS_TO_BACK[@]};do
  copy_volumes $container
  clear_space
done
