#Setup master and slave

aws ec2 run-instances \
    --image-id ami-0f1026b68319bad6c \
    --key-name frankfurt-key-amazon \
    --instance-type t2.micro \
    --region eu-central-1 \
    --security-group-ids sg-4df2c82b \
    --count 1 \
