#!/bin/bash

sed -i "s/.*PasswordAuthentication.*/PasswordAuthentication yes/g" /etc/ssh/sshd_config
systemctl restart sshd.service
echo 'admin:admin' | chpasswd